package com.timothyscarter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jsoup.select.Elements;

public class GetFile {
	
	static String[] Month = { "January", "February", "March", "April", "May", "June", 
		"July", "August", "September", "October", "November", "December" };
	static String[] Day = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
	
	static List<String> sites = new ArrayList<String>();
	static List<String> siteSearch = new ArrayList<String>();
	static List<String> siteSource = new ArrayList<String>();
	static List<String> siteSize = new ArrayList<String>();

	public static Path getHome() {
		Path homeDir = Paths.get(System.getProperty("user.home") + "/Documents/Comics");
		return (homeDir);
	}
	
	public static void CreateHome() {
		Path homeDir = getHome();
		try {
			Files.createDirectory(homeDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void Create_File(List<Elements> image) {
		Path homeDir = getHome();
		Calendar today = Calendar.getInstance();
		int ThisMonth = (today.get(Calendar.MONTH));
		String This_Month = Month[ThisMonth];
		ThisMonth++;
		int thisDay = (today.get(Calendar.DAY_OF_WEEK) - 1);		// Move Sunday to 0 for String
		String ThisDay = Day[thisDay];
		int Today = (today.get(Calendar.DAY_OF_MONTH));
		int ThisYear = (today.get(Calendar.YEAR));
		int Year = (ThisYear % 100);
		try {
			Writer OutputFile = new BufferedWriter
					(new OutputStreamWriter(new FileOutputStream(homeDir + "/Comics_" + ThisMonth + "-" + Today + "-" + Year + ".html")));
			OutputFile.append("\n<HEAD><TITLE>Comics page for: " + ThisMonth + "/" + Today + "/" + Year + "</TITLE></HEAD>");
			OutputFile.append("\n<BODY><CENTER><H1>Today's Comics for : " + ThisDay +", " + This_Month + " " + Today + ", " + ThisYear + "</H1>");
			
			for (int temp=0; temp<image.size(); temp++) {
				if (thisDay == 0) {		// If it's Sunday, we don't need the title since it's already in the strip
					OutputFile.append("\n<p></p> \n<p></p>");	// Just insert a couple spaces on Sunday
				} else {
					OutputFile.append("\n<h2>" + image.get(temp).attr("alt") + "</h2>");
				}
				OutputFile.append("\n<img alt='" + image.get(temp).attr("alt") + siteSource.get(temp) 
						+ image.get(temp).attr("src") + siteSize.get(temp) + "/>");
			}
			
			OutputFile.append("\n</CENTER></BODY>");
			OutputFile.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static List<String> read_Sites() throws IOException {
		Calendar today = Calendar.getInstance();
		int Today = (today.get(Calendar.DAY_OF_WEEK));
		Path homeDir = getHome();
		FileReader url_reader = null;
		if (Today == 1) {			// Is it Sunday?
			url_reader = new FileReader(homeDir.toString() + "/Comic_Sun.txt");
		} else if (Today == 7) {		// Is it Saturday?
			url_reader = new FileReader(homeDir.toString() + "/Comic_Sat.txt");
		} else {
			url_reader = new FileReader(homeDir.toString() + "/Comic.txt");
		}
		BufferedReader url_buffer = new BufferedReader(url_reader);
		String site = null;
		while ((site = url_buffer.readLine()) != null) {
			sites.add(site);
		}
		url_buffer.close();
		return sites;		
	}
	
	public static List<String> read_Search() throws IOException {
		Calendar today = Calendar.getInstance();
		int Today = (today.get(Calendar.DAY_OF_WEEK));
		Path homeDir = getHome();
		FileReader url_reader = null;
		if (Today == 1) {			// Is it Sunday?
			url_reader = new FileReader(homeDir.toString() + "/ComicSearch_Sun.txt");
		} else if (Today == 7) {	// Is it Saturday?
			url_reader = new FileReader(homeDir.toString() + "/ComicSearch_Sat.txt");
		} else {
			url_reader = new FileReader(homeDir.toString() + "/ComicSearch.txt");
		}
		BufferedReader url_buffer = new BufferedReader(url_reader);
		String site_Search = null;
		while ((site_Search = url_buffer.readLine()) != null) {
			siteSearch.add(site_Search);
		}
		url_buffer.close();
		return siteSearch;		
	}
	
	public static void read_Source() throws IOException {
		Calendar today = Calendar.getInstance();
		int Today = (today.get(Calendar.DAY_OF_WEEK));
		Path homeDir = getHome();
		FileReader url_reader = null;
		if (Today == 1) {			// Is it Sunday?
			url_reader = new FileReader(homeDir.toString() + "/ComicSource_Sun.txt");
		} else if (Today == 7) {	// Is it Saturday?
			url_reader = new FileReader(homeDir.toString() + "/ComicSource_Sat.txt");
		} else {
			url_reader = new FileReader(homeDir.toString() + "/ComicSource.txt");
		}
		BufferedReader url_buffer = new BufferedReader(url_reader);
		String site_Source = null;
		while ((site_Source = url_buffer.readLine()) != null) {
			siteSource.add(site_Source);
		}
		url_buffer.close();
	}
	
	public static void read_Size() throws IOException {
		Calendar today = Calendar.getInstance();
		int Today = (today.get(Calendar.DAY_OF_WEEK));
		Path homeDir = getHome();
		FileReader url_reader = null;
		if (Today == 1) {			// Is it Sunday?
			url_reader = new FileReader(homeDir.toString() + "/ComicSize_Sun.txt");
		} else if (Today == 7) {	// Is it Saturday?
			url_reader = new FileReader(homeDir.toString() + "/ComicSize_Sat.txt");
		} else {
			url_reader = new FileReader(homeDir.toString() + "/ComicSize.txt");
		}
		BufferedReader url_buffer = new BufferedReader(url_reader);
		String site_Size = null;
		while ((site_Size = url_buffer.readLine()) != null) {
			siteSize.add(site_Size);
		}
		url_buffer.close();
	}
}

