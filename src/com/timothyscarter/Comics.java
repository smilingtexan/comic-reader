package com.timothyscarter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Comics {
	
	static List<String> sites = new ArrayList<String>();
	static List<Document> doc = new ArrayList<Document>();
	static List<String> siteSearch = new ArrayList<String>();
	static List<Elements> images = new ArrayList<Elements>();
	
  public static void main(String[] args){
	  Path homeDir = GetFile.getHome();
	  if (Files.notExists(homeDir)) {
			System.err.println("Error! Home Directory does not exist.");
			System.out.println("Creating...");
			GetFile.CreateHome();
	  }
	  
	  try {
		  sites = GetFile.read_Sites();
		  siteSearch = GetFile.read_Search();
		  GetFile.read_Source();
		  GetFile.read_Size();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	for (int temp=0; temp<sites.size(); temp++) {
		try {
			doc.add(Jsoup.connect(sites.get(temp)).get());
			images.add(doc.get(temp).select(siteSearch.get(temp)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GetFile.Create_File(images);
	}
  }

}