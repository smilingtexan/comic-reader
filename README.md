# Comic Reader
## by: Timothy Carter (aka: SmilingTexan)

This is a project I worked on some time ago. It's a fun project to retrieve comic images
from various comic sites.

It won't work straight up, because you will need to provide 4 sets of distinct .txt files; one for Mon-Fri, one for Sat and one for Sun. I used separate files for Saturday and Sunday, because at the time many of the sites actually had a different
format for those days. You will need to do your own research on exactly what sites
to add. "Comic.txt" is for the URLs; "ComicSearch" is for the search term in the
html source of the site; "ComicSource" is for added URL strings that may be needed
for the image "ComicSize" is the size of the new comic.

This will save the images into a new html file.
```
<home directory>/Documents/Comics

Comic.txt
Comic_Sat.txt
Comic_Sun.txt

ComicSearch.txt
ComicSearch_Sat.txt
ComicSearch_Sun.txt

ComicSource.txt
ComicSource_Sat.txt
ComicSource_Sun.txt

ComicSize.txt
ComicSize_Sat.txt
ComicSize_Sun.txt
```

The program will create a file (in the form):
```
<home directory>/Comics_01-14-2019.html
```
